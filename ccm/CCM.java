package ccm;

import ccm.component.AboutJDialog;
import ccm.component.ToolBarJPanel;
import ccm.component.image.ImageJPanel;
import ccm.component.image.ManualResizeableImageJPanel;
import ccm.component.image.ResizeableImageJPanel;
import ccm.component.oscilloscope.Test;
import ccm.component.override.JFrame;
import ccm.component.override.TestJFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CCM extends JFrame implements ActionListener
{
    private JPanel        centerJPanel;
    private CardLayout    centerJPanelCardLayout;
    private ToolBarJPanel toolBarJPanel;
    private String[]      nameList;
    private AboutJDialog  aboutJDialog;

    public CCM()
    {
        super();
        setLayout(new BorderLayout());
        addMenuBar();


        centerJPanel=new JPanel();
        add(centerJPanel,BorderLayout.CENTER);
        centerJPanel.setLayout(centerJPanelCardLayout=new CardLayout());

        // toolBarJPanel=new ToolBarJPanel();
        // addButtons(toolBar);
        // //创建一个文本域，用来输出一些信息
        // textArea=new JTextArea(15, 30);
        // textArea.setEditable(false);
        // JScrollPane scrollPane=new JScrollPane(textArea);
        // //把组件添加到面板中
        // setPreferredSize(new Dimension(450, 110));
        // add(toolBar,BorderLayout.PAGE_START);


        // add(toolBarJPanel,BorderLayout.PAGE_START);
        // toolBarJPanel.setLayout(new FlowLayout(FlowLayout.LEFT,5,5));
        // toolBarJPanel.setBackground(new Color(0X33,0X33,0X33));
        // setBackground(new Color(0X92,0XE8,0X0C));
        // setForeground(new Color(0X00,0X00,0XFF));
        // setMargin(new Insets(2,2,2,2));
        // setFocusPainted(false);


        nameList   =new String[2];
        nameList[0]="Monitor";
        centerJPanel.add(new ManualResizeableImageJPanel(ImageJPanel.getTestImage(240,320)),nameList[0]);

        nameList[1]="设置";
        centerJPanel.add(new ResizeableImageJPanel(ImageJPanel.getTestImage(320,240)),nameList[1]);
        add(toolBarJPanel=new ToolBarJPanel(nameList,this),BorderLayout.PAGE_START);


        aboutJDialog=new AboutJDialog(this);


        setVisible(true);

    }

    private void addMenuBar()
    {
        JMenuBar jMenuBar=new JMenuBar();
        setJMenuBar(jMenuBar);
        jMenuBar.setBorderPainted(false);

        JMenu jMenu0=new JMenu("帮助");
        jMenuBar.add(jMenu0);
        JMenuItem aboutJMenuItem=new JMenuItem("关于");
        jMenu0.add(aboutJMenuItem);
        aboutJMenuItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                aboutJDialog.setVisible(true);
            }
        });

        JMenu testJMenu=new JMenu("测试");
        jMenu0.add(testJMenu);
        addTest(testJMenu,new ccm.component.image.Test());
        addTest(testJMenu,new ccm.communicate.serial.Test());
        addTest(testJMenu,new Test());
    }

    public void addTest(JMenu testJMenu,TestJFrame testJFrame)
    {
        JMenuItem jMenuItem=new JMenuItem(getTitleString(testJFrame.getTitleArray()));
        testJMenu.add(jMenuItem);
        testJFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        jMenuItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                testJFrame.setVisible(!testJFrame.isVisible());
            }
        });
    }

    public static void main(String[] args)
    {
        if(args.length==0)
            new ccm.component.style.Style().set();
        else
            switch(args[0])
            {
                case "Jry":
                    new ccm.component.style.Style().set();
                    break;
                case "Blue":
                    new ccm.component.style.Blue().set();
                    break;
            }
        new CCM();
    }

    // public void restart()
    // {
    // this.dispose();
    // new CCM();
    // }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() instanceof ToolBarJPanel)
        {
            String name=e.getActionCommand();
            centerJPanelCardLayout.show(centerJPanel,name);
            this.setTitle(name);
            if(toolBarJPanel!=null)
            {
                toolBarJPanel.setSelected(name);
            }
        }
        // if(e.getSource() instanceof JButton)
        // {
        // for(int i=0;i<jButtons.length;++i)
        // if(e.getSource().equals(jButtons[i]))
        // {
        // switchTo(i);
        // break;
        // }
        // }
    }
}
