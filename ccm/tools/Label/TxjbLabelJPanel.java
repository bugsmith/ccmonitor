package ccm.tools.Label;

import ccm.common.TooManyListenerException;
import ccm.component.override.VFlowLayout;

import javax.swing.*;
import java.awt.*;

public class TxjbLabelJPanel extends PointLabelJPanel
{

    static private final Color[] colors=new Color[]{
            Color.RED,Color.YELLOW,Color.BLUE,Color.MAGENTA,Color.ORANGE,Color.GREEN,Color.PINK,Color.LIGHT_GRAY,new Color(0XD3,0X61,0X35),new Color(112,152,95),new Color(86,81,55),new Color(0XE6,0Xaa,0X68),

            new Color(0X99,0X33,0XFF),new Color(0X00,0X99,0X99),new Color(0X99,0X99,0X33),new Color(114,234,47),new Color(245,7,115),new Color(0X00,0X66,0X33),new Color(0X99,0XCC,0X33),new Color(0XCC,0X66,0X99),
            };


    public TxjbLabelJPanel(SubsetInfo subsetInfo) throws TooManyListenerException
    {
        super(subsetInfo,16*20);


        JPanel jPanel=new JPanel();
        add(jPanel,BorderLayout.EAST);
        jPanel.setLayout(new VFlowLayout());

        JSpinner numJSpinner=new JSpinner(new SpinnerNumberModel(0,0,15,1));
        jPanel.add(numJSpinner);
        numJSpinner.setFont(new Font("Console",Font.PLAIN,20));
        numJSpinner.addChangeListener(e->pointsCnt=((int)(numJSpinner.getValue()))*16);

        JButton clearJButton=new JButton("清空当前层");
        jPanel.add(clearJButton);
        clearJButton.setFont(new Font("Console",Font.PLAIN,20));
        clearJButton.addActionListener(e->{
            pointsCnt=((int)(numJSpinner.getValue()))*16;
            for(int i=pointsCnt;i<pointsCnt+16;++i)
                points[i]=new Point(-1,-1);
            imgId=displayOnly(imgId);
        });
        JButton saveJButton=new JButton("保存");
        jPanel.add(saveJButton);
        saveJButton.setFont(new Font("Console",Font.PLAIN,20));
        saveJButton.addActionListener(e->save());


    }

    Color getColor(int i)
    {
        return colors[i/16];
    }
}
