package ccm.tools.Label;

import javax.swing.*;
import java.awt.*;

public abstract class LabelJPanel extends JPanel
{
    protected static int         imgId=0;
    protected final  JSpinner    imgIdJSpinner;
    protected final  JButton     startJButton;
    protected final  JPanel      mainJPanel;
    protected final  JPanel      northJPanel;
    protected        SubsetInfo  subsetInfo;
    protected        FreshThread thread;

    LabelJPanel(SubsetInfo subsetInfoIn)
    {
        subsetInfo=subsetInfoIn;
        setLayout(new BorderLayout());

        northJPanel=new JPanel();
        northJPanel.setLayout(new FlowLayout());
        add(northJPanel,BorderLayout.NORTH);
        JLabel dirJLabel=new JLabel(subsetInfo.getDir());
        dirJLabel.setFont(new Font("Console",Font.PLAIN,20));
        northJPanel.add(dirJLabel);

        imgIdJSpinner=new JSpinner();
        imgIdJSpinner.addChangeListener(e->imgId=displayOne((int)imgIdJSpinner.getValue()));
        imgIdJSpinner.setFont(new Font("Console",Font.PLAIN,20));
        imgIdJSpinner.setModel(new SpinnerNumberModel(subsetInfoIn.getImageFirst(),subsetInfoIn.getImageFirst(),subsetInfoIn.getImageLast(),1));
        northJPanel.add(imgIdJSpinner);

        JLabel totalJLabel=new JLabel();
        totalJLabel.setFont(new Font("Console",Font.PLAIN,20));
        totalJLabel.setText("/"+subsetInfoIn.getImageLast());
        northJPanel.add(totalJLabel);

        startJButton=new JButton("自动");
        northJPanel.add(startJButton);
        startJButton.setFont(new Font("Console",Font.PLAIN,20));
        startJButton.addActionListener(e->{
            if(e.getSource().equals(startJButton))
            {
                String text=((JButton)e.getSource()).getText();
                if(text.equals("自动"))
                {
                    ((JButton)e.getSource()).setText("停止");
                    if(thread!=null)
                    {
                        thread.interrupt();
                        thread=null;
                    }
                    thread=new FreshThread();
                    thread.start();
                }
                else if(text.equals("停止"))
                {
                    ((JButton)e.getSource()).setText("自动");
                    if(thread!=null)
                    {
                        thread.interrupt();
                        thread=null;
                    }
                }
            }
        });

        mainJPanel=new JPanel();
        add(mainJPanel,BorderLayout.CENTER);
        imgId=subsetInfo.getImageFirst();
    }

    protected abstract int displayOne(int imgId);

    protected class FreshThread extends Thread
    {
        public void run()
        {
            while(true)
            {
                try
                {
                    imgId=displayOne(imgId+1);
                    sleep(1);
                }catch(Exception e)
                {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }


}


