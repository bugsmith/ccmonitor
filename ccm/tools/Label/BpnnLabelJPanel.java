package ccm.tools.Label;

import ccm.common.BufferManager;
import ccm.common.TooManyListenerException;
import ccm.component.image.AutoResizeableImageJPanel;
import ccm.component.image.ImageEvent;
import ccm.component.image.ImageJPanelListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class BpnnLabelJPanel extends LabelJPanel implements ImageJPanelListener
{
    protected final AutoResizeableImageJPanel originImageJPanel;
    protected final AutoResizeableImageJPanel tagImageJPanel;
    protected final JLabel                    hasJLabel;
    protected       int[][]                   tagArray;

    public BpnnLabelJPanel(SubsetInfo subsetInfo) throws TooManyListenerException
    {
        super(subsetInfo);

        mainJPanel.setLayout(new GridLayout(1,2));

        mainJPanel.add(originImageJPanel=new AutoResizeableImageJPanel());
        mainJPanel.add(tagImageJPanel=new AutoResizeableImageJPanel());

        originImageJPanel.addImageListener(this);
        tagImageJPanel.addImageListener(this);

        originImageJPanel.setResizeRate(8);
        tagImageJPanel.setResizeRate(8);

        northJPanel.add(hasJLabel=new JLabel(),BorderLayout.SOUTH);
        hasJLabel.setFont(new Font("Console",Font.PLAIN,20));

        imgId=displayOne(imgId);
        setVisible(true);
    }


    protected int displayOne(int imgId)
    {
        imgId=Math.min(Math.max(imgId,subsetInfo.getImageFirst()),subsetInfo.getImageLast());

        try
        {
            imgIdJSpinner.setValue(imgId);
            System.out.println("imgId:"+imgId);
            File file=new File(subsetInfo.getDir()+"/image"+imgId+".tag");
            byte[] tagBuf=new byte[(int)file.length()];
            try
            {
                FileInputStream in=new FileInputStream(file);
                in.read(tagBuf);
                in.close();
            }catch(IOException e)
            {
                e.printStackTrace();
            }

            BufferedImage originImage=new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_sRGB),null).filter(ImageIO.read(new File(subsetInfo.getDir()+"/image"+imgId+".bmp")),null);
            int width=originImage.getWidth();
            int height=originImage.getHeight();
            BufferedImage tagImage=new BufferedImage(BufferManager.getUint16(tagBuf,0),BufferManager.getUint16(tagBuf,2),BufferedImage.TYPE_INT_RGB);
            int ans=BufferManager.getUint16(tagBuf,4);
            int blank=BufferManager.getUint16(tagBuf,6);
            if(tagImage.getWidth()!=width&&tagImage.getHeight()!=height)
            {
                JOptionPane.showMessageDialog(null,"不正确的尺寸","错误",JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
            tagArray=new int[height][width];
            for(int i=0;i<height;++i)
                for(int j=0;j<width;++j)
                {
                    tagArray[i][j]=BufferManager.getUint16(tagBuf,8+2*(i*width+j));

                    if(tagArray[i][j]==ans&&ans!=blank)
                    {
                        originImage.setRGB(j,i,originImage.getRGB(j,i)&0X00FF00);
                        tagImage.setRGB(j,i,0X00FF00);
                    }
                    else if(tagArray[i][j]!=blank)
                        tagImage.setRGB(j,i,0XFFFFFF);
                }
            hasJLabel.setText(ans==blank?"无":"有");

            originImageJPanel.setImage(originImage);
            tagImageJPanel.setImage(tagImage);

        }catch(IOException e)
        {
            JOptionPane.showMessageDialog(null,"缺少文件","错误",JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            System.exit(0);
        }


        return imgId;
    }

    public void imageClicked(ImageEvent e)
    {
        System.out.println(tagArray[e.getY()][e.getX()]);
        try
        {
            int width=originImageJPanel.getImage().getWidth();
            int height=originImageJPanel.getImage().getHeight();
            byte[] tagBuf=new byte[8+width*height*2];
            BufferManager.setUint16(tagBuf,0,width);
            BufferManager.setUint16(tagBuf,2,height);
            BufferManager.setUint16(tagBuf,4,tagArray[e.getY()][e.getX()]);
            BufferManager.setUint16(tagBuf,6,0XFFFF);


            for(int i=0;i<height;++i)
                for(int j=0;j<width;++j)
                    BufferManager.setUint16(tagBuf,8+(i*width+j)*2,tagArray[i][j]);

            FileOutputStream in=new FileOutputStream(subsetInfo.getDir()+"/image"+imgId+".tag");
            in.write(tagBuf);
            in.close();

        }catch(IOException ex)
        {
            ex.printStackTrace();
        }
        ++imgId;
        displayOne(imgId);
    }
}
