package ccm.tools.Label;

import ccm.common.TooManyListenerException;

import java.awt.*;

public class NtsLabelJPanel extends PointLabelJPanel
{


    static private final Color[] colors=new Color[]{

            new Color(0xFF0000),new Color(0x00FF00),new Color(0x0000FF),new Color(0x00FFFF),
            };

    public NtsLabelJPanel(SubsetInfo subsetInfo) throws TooManyListenerException
    {
        super(subsetInfo,4);
    }

    Color getColor(int i)
    {
        return colors[i%colors.length];
    }
}
