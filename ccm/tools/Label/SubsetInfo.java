package ccm.tools.Label;

import java.io.File;
import java.util.Scanner;

public class SubsetInfo
{
    private final String dir;
    private final int    imageFirst;
    private final int    imageLast;
    private final int    deviceId;
    private final String type;

    public SubsetInfo(String dir) throws Exception
    {
        this.dir=dir;
        Scanner scanner=new Scanner(new File(dir+"/subset.info"));
        if(!scanner.hasNextInt())
            throw new Exception();
        imageFirst=scanner.nextInt();
        if(!scanner.hasNextInt())
            throw new Exception();
        imageLast=scanner.nextInt();
        if(!scanner.hasNextInt())
            throw new Exception();
        deviceId=scanner.nextInt();
        if(!scanner.hasNext())
            throw new Exception();
        type=scanner.next();

        if((!"run".equals(type))&&(!"pos".equals(type))&&(!"neg".equals(type))&&(!"map".equals(type))&&(!"txjb".equals(type))&&(!"nts".equals(type))&&(!"disL".equals(type))&&(!"disR".equals(type))&&(!"dis".equals(type))&&(!"test".equals(type)))
            throw new Exception();
    }

    public String getDir()
    {
        return dir;
    }

    public int getImageFirst()
    {
        return imageFirst;
    }

    public int getImageLast()
    {
        return imageLast;
    }

    public int getDeviceId()
    {
        return deviceId;
    }

    public String getType()
    {
        return type;
    }
}
