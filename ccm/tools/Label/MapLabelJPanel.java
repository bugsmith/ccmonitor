package ccm.tools.Label;

import ccm.common.TooManyListenerException;
import ccm.component.image.ImageJPanelListener;

import javax.swing.*;
import java.awt.*;

public class MapLabelJPanel extends BpnnLabelJPanel implements ImageJPanelListener
{
    protected final JButton jButton;

    public MapLabelJPanel(SubsetInfo subsetInfo) throws TooManyListenerException
    {
        super(subsetInfo);

        northJPanel.add(jButton=new JButton("标记为第一次没灯"),BorderLayout.SOUTH);
        jButton.setFont(new Font("Console",Font.PLAIN,20));

    }


}
