package ccm.tools.Label;

import ccm.component.override.TestJFrame;

import javax.swing.*;
import java.io.File;

public class Label extends TestJFrame
{
    private static final String[] title=new String[]{"tools","Label"};

    public Label()
    {
        super(title);

        setVisible(true);

        JFileChooser fileChooser=new JFileChooser("../code/VM/data/beacon/origin");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal=fileChooser.showOpenDialog(this);
        if(returnVal==JFileChooser.APPROVE_OPTION)
        {
            String fileSaverDir=fileChooser.getSelectedFile().getAbsolutePath();
            System.out.println(fileSaverDir);
            try
            {
                File folder1=new File(fileSaverDir);
                if(!(folder1.exists()&&folder1.isDirectory()))
                    throw new Exception();
            }catch(Exception e)
            {
                JOptionPane.showMessageDialog(this,"缺少文件夹","错误",JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                System.exit(0);
            }
            SubsetInfo subsetInfo=null;
            try
            {
                subsetInfo=new SubsetInfo(fileSaverDir);
            }catch(Exception e)
            {
                JOptionPane.showMessageDialog(this,"文件信息读取错误","错误",JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                System.exit(0);
            }
            try
            {
                switch(subsetInfo.getType())
                {
                    case "run":
                    case "pos":
                    case "dis":
                    case "disL":
                    case "disR":
                        add(new BpnnLabelJPanel(subsetInfo));
                        break;
                    case "map":
                        add(new MapLabelJPanel(subsetInfo));
                        break;
                    case "nts":
                        add(new NtsLabelJPanel(subsetInfo));
                        break;
                    case "txjb":
                        add(new TxjbLabelJPanel(subsetInfo));
                        break;
                    default:
                        JOptionPane.showMessageDialog(this,"不需要标注","错误",JOptionPane.ERROR_MESSAGE);
                        System.exit(0);
                }
            }catch(Exception e)
            {
                JOptionPane.showMessageDialog(this,"启动失败","错误",JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                System.exit(0);
            }

            invalidate();
            validate();
            repaint();
        }
    }

    public static void main(String[] args)
    {
        new Label();
    }

    public String[] getTitleArray()
    {
        return title;
    }

}
