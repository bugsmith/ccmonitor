package ccm.component.style;

import java.awt.*;

public class Blue extends Style
{

    public Blue()
    {
        background    =new Color(0XF2,0XF2,0XF2);
        darkBackground=new Color(0XE2,0XE2,0XE2);
        foreground    =new Color(0X33,0X33,0X33);


        // button_border=BorderFactory.createEmptyBorder(10,20,10,20);

        menuBar_background         =darkBackground;
        menuBar_foreground         =foreground;
        menuBar_selectionBackground=new Color(161,161,161);
        menuBar_selectionForeground=menuBar_foreground;
        // menuBar_border             =BorderFactory.createEmptyBorder(3,6,3,6);
        // menuBar_popUpBorder        =BorderFactory.createEmptyBorder();


        ccm_toolBar_background      =darkBackground;
        ccm_toolBar_foreground      =foreground;
        ccm_toolBar_hoverBackground =new Color(161,161,161);
        ccm_toolBar_hoverForeground =ccm_toolBar_foreground;
        ccm_toolBar_selectBackground=new Color(0X58,0Xb3,0XFE);
        ccm_toolBar_selectForeground=ccm_toolBar_foreground;
        // ccm_toolBar_border          =BorderFactory.createEmptyBorder(14,20,14,20);
    }
}
