package ccm.component.style;

import ccm.common.Configurable;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Enumeration;

public class Style implements Configurable
{
    protected Font  font          =new Font("文泉驿等宽正黑",Font.PLAIN,16);
    protected Color background    =new Color(0X99,0X99,0X99);
    protected Color darkBackground=new Color(0X33,0X33,0X33);
    protected Color foreground    =new Color(0XF2,0XF2,0XF2);

    protected Border button_border=BorderFactory.createEmptyBorder(10,20,10,20);

    protected Color  menuBar_background         =darkBackground;
    protected Color  menuBar_selectionForeground=menuBar_background;
    protected Color  menuBar_foreground         =foreground;
    protected Color  menuBar_selectionBackground=menuBar_foreground;
    protected Border menuBar_border             =BorderFactory.createEmptyBorder(3,6,3,6);
    protected Border menuBar_popUpBorder        =BorderFactory.createEmptyBorder();


    protected Color  ccm_toolBar_background      =darkBackground;
    protected Color  ccm_toolBar_hoverForeground =ccm_toolBar_background;
    protected Color  ccm_toolBar_foreground      =foreground;
    protected Color  ccm_toolBar_hoverBackground =ccm_toolBar_foreground;
    protected Color  ccm_toolBar_selectForeground=ccm_toolBar_foreground;
    protected Color  ccm_toolBar_selectBackground=new Color(0X4C,0XAF,0X50);
    protected Border ccm_toolBar_border          =BorderFactory.createEmptyBorder(14,20,14,20);


    public void set()
    {
        for(Enumeration<Object> keys=UIManager.getDefaults().keys();keys.hasMoreElements();)
        {
            Object key=keys.nextElement();
            Object value=UIManager.get(key);
            if(value instanceof FontUIResource)
            {
                UIManager.put(key,font);
            }
            if(key instanceof String)
            {
                // System.out.println(key);
                if(((String)key).endsWith(".background"))
                    UIManager.put(key,background);
                else if(((String)key).endsWith(".foreground"))
                    UIManager.put(key,foreground);
                // else if(((String)key).endsWith(".border"))
                // UIManager.put(key, BorderFactory.createEmptyBorder());
            }
        }
        UIManager.put("MenuBar.background",menuBar_background);
        UIManager.put("Menu.foreground",menuBar_foreground);
        UIManager.put("Menu.selectionBackground",menuBar_selectionBackground);
        UIManager.put("Menu.selectionForeground",menuBar_selectionForeground);
        UIManager.put("Menu.border",menuBar_border);

        UIManager.put("MenuItem.foreground",menuBar_foreground);
        UIManager.put("MenuItem.background",menuBar_background);
        UIManager.put("MenuItem.selectionBackground",menuBar_selectionBackground);
        UIManager.put("MenuItem.selectionForeground",menuBar_selectionForeground);
        UIManager.put("MenuItem.border",menuBar_border);

        UIManager.put("PopupMenu.background",menuBar_background);
        UIManager.put("PopupMenu.border",menuBar_popUpBorder);


        UIManager.put("Button.border",button_border);

        UIManager.put("Ccm.ToolBar.background",ccm_toolBar_background);
        UIManager.put("Ccm.ToolBar.foreground",ccm_toolBar_foreground);
        UIManager.put("Ccm.ToolBar.hoverForeground",ccm_toolBar_hoverForeground);
        UIManager.put("Ccm.ToolBar.hoverBackground",ccm_toolBar_hoverBackground);
        UIManager.put("Ccm.ToolBar.selectForeground",ccm_toolBar_selectForeground);
        UIManager.put("Ccm.ToolBar.selectBackground",ccm_toolBar_selectBackground);
        UIManager.put("Ccm.ToolBar.border",ccm_toolBar_border);
    }

    public JPanel getConfigJPanel()
    {
        return null;
    }

    // static void

    public void writeExternal(ObjectOutput out) throws IOException
    {
        // int n=0;
        // for(Enumeration<Object> keys=UIManager.getDefaults().keys();keys.hasMoreElements();)
        // {
        // Object key  =keys.nextElement();
        // Object value=UIManager.get(key);
        // if(key instanceof String&&value instanceof Serializable&&!(value.getClass().isArray()))
        // ++n;
        // }
        // out.writeInt(n);
        // for(Enumeration<Object> keys=UIManager.getDefaults().keys();keys.hasMoreElements();)
        // {
        // Object key  =keys.nextElement();
        // Object value=UIManager.get(key);
        // if(key instanceof String&&value instanceof Serializable&&!(value.getClass().isArray()))
        // {
        // System.out.println(key);
        // out.writeObject(key);
        // out.writeObject(value);
        // }
        // }
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
    {
        // int n=in.readInt();
        // for(int i=0;i<n;++i)
        // UIManager.put(in.readObject(),in.readObject());
    }
}
