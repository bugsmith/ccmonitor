package ccm.component.input;

import ccm.common.ReceiveDataEvent;
import ccm.common.StatusChangeEvent;
import ccm.component.override.VFlowLayout;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class MultipleInputJPanel extends InputJPanel implements ComponentListener, CaretListener
{
    /**
     * 列数
     */
    protected final static int          rows=7;
    /**
     * 行数
     */
    protected final static int          cols=3;
    /**
     * 中间的主面板
     * 用于放发送按钮和输入框
     */
    protected final        JPanel       centerJPanel;
    /**
     * 清空按钮
     */
    protected final        JButton      cleanJButton;
    /**
     * 保存输入的内容
     */
    protected              JTextField[] jTextFields;
    /**
     * 发送按钮
     */
    protected              JButton[]    sendJButtons;

    public MultipleInputJPanel()
    {
        super();
        addComponentListener(this);
        setLayout(new BorderLayout());

        /*
         生成左边的配置栏
         */
        JPanel configJPanel=new JPanel();
        add(configJPanel,BorderLayout.EAST);
        configJPanel.setLayout(new VFlowLayout(VFlowLayout.TOP));
        configJPanel.add(endJComboBox);
        cleanJButton=new JButton("清空");
        configJPanel.add(cleanJButton);
        cleanJButton.addActionListener(this);

        /*
         生成中间的主面板
         */
        centerJPanel=new JPanel();
        add(centerJPanel);
        centerJPanel.setLayout(new GridLayout(rows,cols));
        jTextFields=new JTextField[rows*cols];
        updateJTextFields();
    }

    /**
     * 重新设置输入框
     */
    private void updateJTextFields()
    {
        centerJPanel.removeAll();
        sendJButtons=new JButton[jTextFields.length];
        for(int i=0;i<jTextFields.length;++i)
        {
            JPanel jPanel=new JPanel();
            centerJPanel.add(jPanel);
            jPanel.setLayout(new FlowLayout(FlowLayout.LEFT,4,4));
            jTextFields[i]=new JTextField();
            jPanel.add(jTextFields[i]);
            jTextFields[i].addActionListener(this);
            jTextFields[i].addCaretListener(this);
            sendJButtons[i]=new JButton(String.format("%d",i));
            jPanel.add(sendJButtons[i]);
            sendJButtons[i].addActionListener(this);
        }
        updateJTextFieldsSize();
    }

    /**
     * 更新输入框的发送按钮的大小
     */
    protected void updateJTextFieldsSize()
    {
        int sendJButtonWidth=0;
        int sendJButtonHeight=0;
        for(int i=0;i<jTextFields.length;++i)
        {
            sendJButtonWidth =Math.max(sendJButtonWidth,(int)(sendJButtons[i].getFont().getSize()*sendJButtons[i].getText().length()*1.2+sendJButtons[i].getInsets().left+sendJButtons[i].getInsets().right));
            sendJButtonHeight=Math.max(sendJButtonHeight,(int)(sendJButtons[i].getFont().getSize()*1.2+sendJButtons[i].getInsets().bottom+sendJButtons[i].getInsets().top));
            sendJButtonHeight=Math.max(sendJButtonHeight,(int)(jTextFields[i].getFont().getSize()*1.5+jTextFields[i].getInsets().bottom+jTextFields[i].getInsets().top));
        }
        Dimension sendJButtonDimension=new Dimension(sendJButtonWidth,sendJButtonHeight);
        Dimension jTextFieldsDimension=new Dimension(centerJPanel.getWidth()/cols-sendJButtonWidth-10,sendJButtonHeight);
        for(int i=0;i<jTextFields.length;++i)
        {
            sendJButtons[i].setPreferredSize(sendJButtonDimension);
            jTextFields[i].setPreferredSize(jTextFieldsDimension);
            sendJButtons[i].revalidate();
            jTextFields[i].revalidate();
        }
        revalidate();
    }

    /**
     * 用于保存参数
     */
    public void writeExternal(ObjectOutput out) throws IOException
    {
        out.writeObject(endJComboBox.getSelectedItem());
        out.writeInt(jTextFields.length);
        for(JTextField jTextField: jTextFields)
            out.writeObject(jTextField.getText());

    }

    /**
     * 用于加载参数
     */
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
    {
        endJComboBox.setSelectedItem(in.readObject());
        int n=in.readInt();
        jTextFields=new JTextField[Math.max(n,cols*rows)];
        updateJTextFields();
        for(int i=0;i<n;++i)
            jTextFields[i].setText((String)in.readObject());

    }

    /**
     * 点击按钮的时候触发发送事件
     */
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(cleanJButton))
        {
            for(JTextField jTextField: jTextFields)
                jTextField.setText("");
        }
        else if(receiveDataListener!=null)
            if(e.getSource() instanceof JButton)
                receiveDataListener.receiveData(new ReceiveDataEvent(this,jTextFields[Integer.parseInt(((JButton)e.getSource()).getText())].getText()+getEndString()));
            else if(e.getSource() instanceof JTextField)
                receiveDataListener.receiveData(new ReceiveDataEvent(this,((JTextField)e.getSource()).getText()+getEndString()));
    }

    /**
     * 输入内容的时候触发保存事件
     */
    public void caretUpdate(CaretEvent e)
    {
        if(e.getSource() instanceof JTextField)
            if(statusChangeListener!=null)
                statusChangeListener.statusChange(new StatusChangeEvent(this,StatusChangeEvent.CONFIG));
    }

    /**
     * 窗口尺寸变化的时候更新输入框的尺寸
     */
    public void componentResized(ComponentEvent e)
    {
        updateJTextFieldsSize();
    }

    public void componentMoved(ComponentEvent e)
    {

    }

    public void componentShown(ComponentEvent e)
    {

    }

    public void componentHidden(ComponentEvent e)
    {

    }

    /**
     * 获取配置用的面板
     *
     * @return 配置用的面板
     */
    public JPanel getConfigJPanel()
    {
        return null;
    }
}
