package ccm.component.image;

import java.awt.event.ActionEvent;

public class ImageEvent extends ActionEvent
{
    /**
     * 点击的横坐标
     */
    protected final int x;
    /**
     * 点击的纵坐标
     */
    protected final int y;

    public ImageEvent(Object source,int id,String command,int x,int y)
    {
        super(source,id,command);
        this.x=x;
        this.y=y;
    }

    public ImageEvent(Object source,int id,String command,int modifiers,int x,int y)
    {
        super(source,id,command,modifiers);
        this.x=x;
        this.y=y;
    }

    public ImageEvent(Object source,int id,String command,long when,int modifiers,int x,int y)
    {
        super(source,id,command,when,modifiers);
        this.x=x;
        this.y=y;
    }

    /**
     * 获取点击的横坐标
     *
     * @return 点击的横坐标
     */
    public int getX()
    {
        return x;
    }

    /**
     * 获取点击的纵坐标
     *
     * @return 点击的纵坐标
     */
    public int getY()
    {
        return y;
    }

    public String toString()
    {
        return "ImageEvent{"+"x="+x+", y="+y+'}';
    }
}
