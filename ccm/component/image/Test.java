package ccm.component.image;

import ccm.common.TooManyListenerException;
import ccm.component.override.TestJFrame;

import javax.swing.border.TitledBorder;
import java.awt.*;

public class Test extends TestJFrame implements ImageJPanelListener
{
    private static final String[] title=new String[]{"ui","image","experiment"};

    public Test()
    {
        super(title);
        setLayout(new GridLayout(2,2));

        ImageJPanel imageJPanel1=new ImageJPanel(ImageJPanel.getTestImage(320,240));
        ImageJPanel imageJPanel2=new ResizeableImageJPanel(ImageJPanel.getTestImage(320,240));
        ImageJPanel imageJPanel3=new AutoResizeableImageJPanel(ImageJPanel.getTestImage(320,240));
        ImageJPanel imageJPanel4=new ManualResizeableImageJPanel(ImageJPanel.getTestImage(320,240));

        add(imageJPanel1);
        add(imageJPanel2);
        add(imageJPanel3);
        add(imageJPanel4);

        imageJPanel1.setBorder(new TitledBorder("ImageJPanel"));
        imageJPanel2.setBorder(new TitledBorder("ResizeableImageJPanel"));
        imageJPanel3.setBorder(new TitledBorder("AutoResizeableImageJPanel"));
        imageJPanel4.setBorder(new TitledBorder("ManualResizeableImageJPanel"));

        try
        {
            imageJPanel1.addImageListener(this);
            imageJPanel2.addImageListener(this);
            imageJPanel3.addImageListener(this);
            imageJPanel4.addImageListener(this);
        }catch(TooManyListenerException e)
        {
            e.printStackTrace();
        }


    }

    public static void main(String[] args)
    {
        new Test().setVisible(true);
    }

    public String[] getTitleArray()
    {
        return title;
    }

    public void imageClicked(ImageEvent e)
    {
        System.out.println(e);
    }
}
