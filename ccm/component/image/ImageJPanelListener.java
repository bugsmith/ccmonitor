package ccm.component.image;

public interface ImageJPanelListener
{
    /**
     * 鼠标点击图像事件
     */
    void imageClicked(ImageEvent e);
}
