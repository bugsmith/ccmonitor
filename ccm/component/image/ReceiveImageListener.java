package ccm.component.image;

public interface ReceiveImageListener
{
    /**
     * 接收到图像事件
     */
    void receiveImage(ReceiveImageEvent event);
}
