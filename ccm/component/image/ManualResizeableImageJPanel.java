package ccm.component.image;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.image.BufferedImage;

public class ManualResizeableImageJPanel extends AutoResizeableImageJPanel implements ChangeListener
{
    protected JSpinner resizeJSpinner;

    public ManualResizeableImageJPanel()
    {
        this(null);
    }

    public ManualResizeableImageJPanel(BufferedImage image)
    {
        super(image);

        setLayout(new BorderLayout());
        JPanel imgJPanel=new JPanel();
        add(imgJPanel);
        imgJPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        imgJPanel.add(imgJLabel,BorderLayout.CENTER);
        imgJPanel.addComponentListener(this);
        resizeRate=1;

        JPanel jPanel=new JPanel();
        add(jPanel,BorderLayout.NORTH);
        jPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        jPanel.add(new JLabel("缩放:"));

        resizeJSpinner=new JSpinner(new SpinnerNumberModel(1,0.1,20,0.1));
        jPanel.add(resizeJSpinner);
        resizeJSpinner.addChangeListener(this);

        updateImg();
    }

    /**
     * 在手动设置缩放的输入框中值发生变化时自动更新缩放
     */
    public void stateChanged(ChangeEvent e)
    {
        if(e.getSource().equals(resizeJSpinner))
            setResizeRate((Double)(resizeJSpinner.getValue()));
    }
}
