package ccm.component.image;

import ccm.common.CCMEvent;

import java.awt.image.BufferedImage;

public class ReceiveImageEvent extends CCMEvent
{
    protected final BufferedImage image;

    /**
     * @param source 事件源
     * @param image  图像
     */
    public ReceiveImageEvent(Object source,BufferedImage image)
    {
        super(source);
        this.image=image;
    }

    /**
     * 获取图像
     *
     * @return 图像
     */
    public BufferedImage getImage()
    {
        return image;
    }
}
