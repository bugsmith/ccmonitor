package ccm.component.image;

import ccm.common.TooManyListenerException;
import ccm.communicate.tcp.TcpClient;
import ccm.component.override.TestJFrame;

import java.awt.*;

public class ReceiveImageTest extends TestJFrame
{
    private static final String[]     title=new String[]{"communicate","tcp","ReceiveImageTest"};
    protected            TcpClient    tcpClient;
    protected            ImageDecoder imageDecoder;
    protected            ImageJPanel  imageJPanel;

    ReceiveImageTest() throws TooManyListenerException
    {
        super(title);
        setLayout(new BorderLayout());

        tcpClient   =new TcpClient();
        imageDecoder=new ImageDecoder();
        tcpClient.addReceiveDataListener(imageDecoder);
        add(imageJPanel=new ManualResizeableImageJPanel());
        imageDecoder.addReceiveImageListener(imageJPanel);


        tcpClient.start();


    }

    public static void main(String[] args) throws TooManyListenerException
    {
        (new ReceiveImageTest()).setVisible(true);
    }

    public String[] getTitleArray()
    {
        return title;
    }
}
