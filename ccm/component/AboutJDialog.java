package ccm.component;

import ccm.component.override.JFrame;

import javax.swing.*;
import java.awt.*;

public class AboutJDialog extends JDialog
{
    public AboutJDialog(java.awt.Frame owner)
    {
        super(owner,JFrame.getTitleString("关于"),true);
        setSize(550,300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setResizable(false);
        setLayout(new FlowLayout());
        JLabel jLabel0=new JLabel("车车Monitor  (1.0dev)");
        JLabel jLabel1=new JLabel("made by 火星大王");
        JLabel jLabel2=new JLabel("欢迎访问蒟蒻云http://juruoyun.top");
        getContentPane().setBackground(new Color(0X00,0XFF,0X00));
        jLabel0.setForeground(new Color(0XFF,0X00,0X00));
        jLabel1.setForeground(new Color(0XFF,0X00,0X00));
        jLabel2.setForeground(new Color(0XFF,0X00,0X00));
        jLabel0.setFont(new Font("",Font.PLAIN,30));
        jLabel1.setFont(new Font("",Font.PLAIN,30));
        jLabel2.setFont(new Font("",Font.PLAIN,30));
        add(jLabel0);
        add(jLabel1);
        add(jLabel2);
        add(new JLabel(new ImageIcon(new ImageIcon(this.getClass().getResource("/ccm/resource/img/huaji.png")).getImage().getScaledInstance(100,100,java.awt.Image.SCALE_SMOOTH))));
        // setVisible(true);
    }
}
