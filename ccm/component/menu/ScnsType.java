package ccm.component.menu;

public class ScnsType
{
    public static final int      INT8    =0;
    public static final int      UINT8   =1;
    public static final int      INT16   =2;
    public static final int      UINT16  =3;
    public static final int      INT32   =4;
    public static final int      FLOAT   =5;
    public static final int      DOUBLE  =6;
    public static final int      FUNCTION=7;
    static final        String[] name    =new String[]{
            "INT8","UINT8","INT16","UINT16","INT32","FLOAT","DOUBLE","FUNCTION",
            };
    private             int      type;

    public ScnsType(int type)
    {
        this.type=type;
    }

    public int getType()
    {
        return type;
    }

    public String toString()
    {
        return name[type];
    }

    public boolean equals(int type)
    {
        return this.type==type;
    }
}