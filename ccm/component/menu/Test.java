package ccm.component.menu;

import ccm.common.*;
import ccm.communicate.serial.Serial;
import ccm.component.override.TestJFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Test extends TestJFrame implements StatusChangeListener, ActionListener, ReceiveDataListener
{
    private static final String[] title=new String[]{"ui","menu","test"};

    private MenuJPanel menuJPanel;
    private Serial     serial;
    private JButton    startJButton;


    public Test()
    {
        super(title);
        setLayout(new BorderLayout());
        add(menuJPanel=new MenuJPanel(),BorderLayout.WEST);
        menuJPanel.a=this;

        JPanel leftJPanel=new JPanel();
        leftJPanel.setLayout(new BorderLayout());
        add(leftJPanel,BorderLayout.EAST);
        serial=new Serial();
        try
        {
            serial.addReceiveDataListener(menuJPanel);
            serial.addStatusChangeListener(this);
        }catch(TooManyListenerException ex)
        {
            ex.printStackTrace();
        }
        leftJPanel.add(serial.getConfigJPanel(),BorderLayout.NORTH);


        JPanel buttonsJPanel=new JPanel();
        leftJPanel.add(buttonsJPanel,BorderLayout.CENTER);
        buttonsJPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        startJButton=new JButton("开始");
        buttonsJPanel.add(startJButton);
        startJButton.addActionListener(this);

    }

    public static void main(String[] args)
    {
        new Test().setVisible(true);
    }

    public String[] getTitleArray()
    {
        return title;
    }

    public void statusChange(StatusChangeEvent e)
    {
        if(e.getSource().equals(serial))
        {
            if(e.getReason()==StatusChangeEvent.START_STOP)
                startJButton.setText(serial.isOpen()?"停止":"开始");
        }
    }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(startJButton))
        {
            if(serial.isOpen())
                serial.stop();
            else
                serial.start();
        }
    }

    public void receiveData(ReceiveDataEvent event)
    {
        // add(new OscilloscopeJPanel(menuJPanel),BorderLayout.CENTER);
    }
}
