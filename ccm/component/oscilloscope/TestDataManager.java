package ccm.component.oscilloscope;

import ccm.common.DataNotMatchException;

import java.util.ArrayList;

public class TestDataManager implements OscilloscopeDataManager
{
    /**
     * 用于保存接收到的数据
     */
    protected final ArrayList<float[]> data;
    protected final ArrayList<Long>    timeUs;

    public TestDataManager()
    {
        data  =new ArrayList<>(1024*128);
        timeUs=new ArrayList<Long>(1024*128);
        new Thread(()->{
            try
            {
                while(true)
                {
                    add();
                    Thread.sleep(1);
                }
            }catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }).start();
    }

    private void add()
    {
        final int i=getDataLength();
        float[] ans=new float[getDataColumn()];
        for(int j=0;j<ans.length;++j)
            ans[j]=(float)Math.sin(i*1e-2+j*2.0/ans.length*Math.PI);
        data.add(ans);
        timeUs.add((long)i*1000);
    }

    /**
     * 获取一共有几列
     *
     * @return 列数
     */
    public int getDataColumn()
    {
        return 20;
    }

    /**
     * 清空数据
     */
    synchronized public void clear()
    {
        data.clear();
        timeUs.clear();
    }

    /**
     * 获取数据行数
     */
    synchronized public int getDataLength()
    {
        if(data.size()!=timeUs.size())
            throw new DataNotMatchException();
        return data.size();
    }

    /**
     * 获取列的名字
     *
     * @param column 列
     * @return 注释
     */
    public String getColumnName(int column)
    {
        return String.format("Data%d",column+1);
    }

    /**
     * 获取第i行数据第column列是不是可用的数据点
     *
     * @param i      数据行
     * @param column 数据列
     * @return 是否可用
     */
    public boolean isColumnEnabled(int i,int column)
    {
        return true;
    }

    /**
     * 获取纵坐标
     *
     * @param i      数据行
     * @param column 数据列
     * @return 纵坐标
     */
    public float getY(int i,int column)
    {
        return data.get(i)[column];
    }

    public long getTimeUs(int i)
    {
        return timeUs.get(i);
    }


    /**
     * 克隆管理器
     *
     * @return 克隆的管理器
     */
    synchronized public TestDataManager clone()
    {
        try
        {
            return (TestDataManager)super.clone();
        }catch(CloneNotSupportedException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}
