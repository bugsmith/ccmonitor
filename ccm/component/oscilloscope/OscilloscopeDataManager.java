package ccm.component.oscilloscope;

public interface OscilloscopeDataManager extends Cloneable
{
    /**
     * 获取一共有几列
     *
     * @return 列数
     */
    int getDataColumn();

    /**
     * 清空数据
     */
    void clear();

    /**
     * 获取数据行数
     */
    int getDataLength();

    /**
     * 克隆管理器
     *
     * @return 克隆的管理器
     */
    OscilloscopeDataManager clone();

    /**
     * 获取列的名字
     *
     * @param column 列
     * @return 注释
     */
    String getColumnName(int column);

    /**
     * 获取第i行数据第column列是不是可用的数据点
     *
     * @param i      数据行
     * @param column 数据列
     * @return 是否可用
     */
    boolean isColumnEnabled(int i,int column);

    /**
     * 获取纵坐标
     *
     * @param i      数据行
     * @param column 数据列
     * @return 纵坐标
     */
    float getY(int i,int column);

    /**
     * 获取时间戳
     *
     * @param i 数据行
     * @return 时间戳，单位为微秒
     */
    long getTimeUs(int i);
}
