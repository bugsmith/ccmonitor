package ccm.component.oscilloscope;

import ccm.common.*;
import ccm.communicate.common.Communicate;
import ccm.communicate.common.NotStartException;
import ccm.communicate.common.SendFailedException;
import ccm.communicate.serial.Serial;
import ccm.component.input.InputJPanel;
import ccm.component.input.MultipleInputJPanel;
import ccm.component.override.TestJFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Test extends TestJFrame implements ActionListener, ReceiveDataListener, StatusChangeListener
{
    private static final String[]               title=new String[]{"communicate","middleware","visualscope","test"};
    private final        JButton                communicateJButton;
    private final        VisualScopeDataManager dataManager;
    private              Communicate            communicate;
    private              InputJPanel            inputJPanel;

    public Test()
    {
        super(title);
        readConfig();
        setLayout(new BorderLayout());

        dataManager=new VisualScopeDataManager();
        this.add(new OscilloscopeJPanel(dataManager),BorderLayout.CENTER);

        if(inputJPanel==null)
            inputJPanel=new MultipleInputJPanel();
        this.add(inputJPanel,BorderLayout.SOUTH);
        try
        {
            inputJPanel.addDataReceiveListener(this);
            inputJPanel.addStatusChangeListener(this);
        }catch(TooManyListenerException e)
        {
            e.printStackTrace();
        }

        JPanel leftJPanel=new JPanel();
        this.add(leftJPanel,BorderLayout.EAST);
        leftJPanel.setLayout(new BorderLayout());

        // if(communicate==null)
        communicate=new Serial();
        try
        {
            communicate.addReceiveDataListener(dataManager);
            communicate.addStatusChangeListener(this);
        }catch(TooManyListenerException e)
        {
            e.printStackTrace();
        }
        leftJPanel.add(communicate.getConfigJPanel(),BorderLayout.NORTH);


        JPanel buttonsJPanel=new JPanel();
        leftJPanel.add(buttonsJPanel,BorderLayout.CENTER);
        buttonsJPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        communicateJButton=new JButton("启动串口");
        buttonsJPanel.add(communicateJButton);
        communicateJButton.addActionListener(this);


    }

    public void readConfig()
    {
        try
        {
            ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream("configs/"+getTitleString()+".config"));
            communicate=(Communicate)objectInputStream.readObject();
            inputJPanel=(InputJPanel)objectInputStream.readObject();
            objectInputStream.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        System.setProperty("sun.java2d.opengl","true");
        new Test().setVisible(true);
    }

    public String[] getTitleArray()
    {
        return title;
    }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(communicateJButton))
        {
            if(communicate.isOpen())
                communicate.stop();
            else
                communicate.start();
        }
    }

    public void receiveData(ReceiveDataEvent e)
    {
        if(e.getSource().equals(communicate))
            System.out.print(new String(e.getData()));
        else if(e.getSource().equals(inputJPanel))
        {
            try
            {
                communicate.send(e.getData());
            }catch(NotStartException ex)
            {
                JOptionPane.showMessageDialog(Test.this,"未开启串口","错误",JOptionPane.ERROR_MESSAGE);

            }catch(SendFailedException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void statusChange(StatusChangeEvent e)
    {
        if(e.getSource().equals(communicate))
        {
            if(e.getReason()==StatusChangeEvent.START_STOP)
            {
                dataManager.clear();
                communicateJButton.setText(communicate.isOpen()?"关闭串口":"启动串口");
            }
        }
        else if(e.getReason()==StatusChangeEvent.CONFIG)
            writeConfig();
    }

    public void writeConfig()
    {
        System.out.println("writeConfig");
        try
        {
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(new FileOutputStream("configs/"+getTitleString()+".config"));
            objectOutputStream.writeObject(communicate);
            objectOutputStream.writeObject(inputJPanel);
            objectOutputStream.close();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
