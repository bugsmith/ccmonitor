package ccm.component.output;

import ccm.common.ReceiveDataEvent;
import ccm.common.ReceiveDataListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class OutputJPanel extends JPanel implements ReceiveDataListener, ComponentListener
{
    /**
     * 输出的文本区
     */
    protected final JTextArea   jTextArea;
    /**
     * 滚动条
     */
    protected final JScrollPane jScrollPane;

    public OutputJPanel()
    {
        jTextArea=new JTextArea();
        // jTextArea=new JTextArea(null,null,100000,512);
        this.add(jScrollPane=new JScrollPane(jTextArea));
        jTextArea.setEditable(false);
        jTextArea.setFont(new Font("Consolas",Font.PLAIN,16));
        this.addComponentListener(this);
        System.out.println(this.getClass().getName());
    }

    /**
     * 接收到数据时更新文本区
     */
    public void receiveData(ReceiveDataEvent event)
    {
        jTextArea.append(new String(event.getData()));
        jScrollPane.getVerticalScrollBar().setValue(jScrollPane.getVerticalScrollBar().getMaximum());
        jTextArea.paintImmediately(jTextArea.getBounds());
    }

    /**
     * 清空文本区
     */
    public void clear()
    {
        jTextArea.setText("");
        jTextArea.paintImmediately(jTextArea.getBounds());
    }

    /**
     * 窗口大小改变时缩放文本区确保充满
     */
    public void componentResized(ComponentEvent e)
    {
        jScrollPane.setPreferredSize(new Dimension(getWidth(),getHeight()));
        revalidate();
    }


    public void componentMoved(ComponentEvent e)
    {

    }

    public void componentShown(ComponentEvent e)
    {

    }

    public void componentHidden(ComponentEvent e)
    {

    }
}
