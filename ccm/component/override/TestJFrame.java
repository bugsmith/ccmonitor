package ccm.component.override;


import java.awt.*;


abstract public class TestJFrame extends JFrame
{
    public TestJFrame(String[] title) throws HeadlessException
    {
        super(title);
    }

    final public String getTitleString()
    {
        return getTitleString(getTitleArray());
    }

    public abstract String[] getTitleArray();
}
