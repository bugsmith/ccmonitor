package ccm.component.override;

import javax.swing.*;
import java.awt.*;

abstract public class JFrame extends javax.swing.JFrame
{
    private static final String allTitle="车车Monitor";

    public JFrame() throws HeadlessException
    {
        this(getTitleString(""));
    }


    public JFrame(String title) throws HeadlessException
    {
        super(getTitleString(title));
        setSize(1400,800);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * 获取标题的字符串
     *
     * @param title 标题
     * @return 标题的字符串
     */
    static public String getTitleString(String title)
    {
        return (title.length()==0)?(allTitle):(allTitle+"|"+title);
    }

    public JFrame(String[] title) throws HeadlessException
    {
        this(getTitleString(title));
    }

    /**
     * 获取标题的字符串
     *
     * @param title 标题
     * @return 标题的字符串
     */
    static public String getTitleString(String[] title)
    {
        StringBuilder ans=new StringBuilder(allTitle);
        for(String s: title)
            ans.append("-").append(s);
        return ans.toString();
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String title)
    {
        super.setTitle(getTitleString(title));
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public void setTitle(String[] title)
    {
        super.setTitle(getTitleString(title));
    }

}
