package ccm.component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ToolBarJPanel extends javax.swing.JPanel implements MouseListener, ActionListener
{
    private String[]       nameList;
    private JButton[]      jButtons;
    private int            selected=0;
    private ActionListener actionListener;


    public ToolBarJPanel(String[] nl)
    {
        this(nl,null);
    }


    public ToolBarJPanel(String[] nl,ActionListener listener)
    {
        super();
        actionListener=listener;
        nameList      =nl;
        setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
        setBackground(UIManager.getColor("Ccm.ToolBar.background"));


        jButtons=new JButton[nameList.length];
        selected=0;
        for(int i=0;i<jButtons.length;++i)
        {
            jButtons[i]=new JButton(nameList[i]);
            add(jButtons[i]);
            setButtonColor(jButtons[i],i==selected,false);
            jButtons[i].setFocusPainted(false);
            jButtons[i].setPressedIcon(null);
            jButtons[i].setBorder(UIManager.getBorder("Ccm.ToolBar.border"));
            jButtons[i].addMouseListener(this);
            jButtons[i].addActionListener(this);
        }
        if(actionListener!=null)
            actionListener.actionPerformed(new ActionEvent(this,1,nameList[selected]));
    }

    private void setButtonColor(JButton jButton,boolean selected,boolean hover)
    {
        if(selected)
        {
            jButton.setBackground(UIManager.getColor("Ccm.ToolBar.selectBackground"));
            jButton.setForeground(UIManager.getColor("Ccm.ToolBar.selectForeground"));
        }
        else if(hover)
        {
            jButton.setBackground(UIManager.getColor("Ccm.ToolBar.hoverBackground"));
            jButton.setForeground(UIManager.getColor("Ccm.ToolBar.hoverForeground"));

        }
        else
        {
            jButton.setBackground(UIManager.getColor("Ccm.ToolBar.background"));
            jButton.setForeground(UIManager.getColor("Ccm.ToolBar.foreground"));
        }
    }

    public void setSelected(String name)
    {
        for(int i=0;i<nameList.length;++i)
            if(name.equals(nameList[i]))
            {
                setButtonColor(jButtons[selected],false,false);
                selected=i;
                setButtonColor(jButtons[selected],true,false);
                break;
            }
    }

    public void addActionListener(ActionListener listener)
    {
        this.actionListener=listener;
    }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() instanceof JButton)
        {
            if(actionListener!=null)
                actionListener.actionPerformed(new ActionEvent(this,1,((JButton)e.getSource()).getText()));
        }
    }

    public void mouseClicked(MouseEvent e)
    {

    }

    public void mousePressed(MouseEvent e)
    {

    }

    public void mouseReleased(MouseEvent e)
    {

    }

    public void mouseEntered(MouseEvent e)
    {
        if(e.getSource() instanceof JButton)
        {
            setButtonColor(((JButton)e.getSource()),((JButton)e.getSource()).getText().equals(nameList[selected]),true);
        }
    }

    public void mouseExited(MouseEvent e)
    {
        if(e.getSource() instanceof JButton)
        {
            setButtonColor(((JButton)e.getSource()),((JButton)e.getSource()).getText().equals(nameList[selected]),false);
        }
    }
}
