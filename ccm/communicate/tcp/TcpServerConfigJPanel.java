package ccm.communicate.tcp;

import ccm.component.override.VFlowLayout;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TcpServerConfigJPanel extends JPanel implements ActionListener, ChangeListener
{
    /**
     * 保存被控制的TCP连接
     */
    final TcpServer tcpServer;
    /**
     * 保存主机端口的输入框
     */
    final JSpinner  portJSpinner;

    TcpServerConfigJPanel(TcpServer tcpServerIn) throws NullPointerException
    {
        super();
        if(tcpServerIn==null)
            throw new NullPointerException();
        tcpServer=tcpServerIn;
        setLayout(new VFlowLayout());
        setBorder(new TitledBorder("TcpServer设置"));

        /*
          生成主机的面板
         */
        JPanel portJSpinnerJPanel=new JPanel();
        add(portJSpinnerJPanel);
        portJSpinnerJPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        portJSpinnerJPanel.add(new JLabel("主机端口"));

        portJSpinner=new JSpinner(new SpinnerNumberModel(tcpServer.getPort(),0,65535,1));
        portJSpinnerJPanel.add(portJSpinner);
        portJSpinner.addChangeListener(this);
    }

    public void actionPerformed(ActionEvent e)
    {

    }

    public void stateChanged(ChangeEvent e)
    {
        if(e.getSource().equals(portJSpinner))
        {
            tcpServer.setPort((Integer)portJSpinner.getValue());
        }
    }
}
