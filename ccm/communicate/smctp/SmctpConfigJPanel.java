package ccm.communicate.smctp;

import ccm.component.override.VFlowLayout;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static ccm.common.Utils.timeFormatter;

public class SmctpConfigJPanel extends JPanel implements ActionListener
{
    /**
     * 保存被控制
     */
    protected final Smctp              smctp;
    /**
     * 通道下拉菜单
     */
    protected final JComboBox<Integer> chanelJComboBox;
    protected final JLabel             timeJLabel;

    SmctpConfigJPanel(Smctp smctpIn) throws NullPointerException
    {
        super();
        if(smctpIn==null)
            throw new NullPointerException();
        this.smctp=smctpIn;
        setLayout(new VFlowLayout());
        setBorder(new TitledBorder("SMCTP设置"));
        /*
          通道的面板
         */
        JPanel chanelJComboBoxJPanel=new JPanel();
        this.add(chanelJComboBoxJPanel);
        chanelJComboBoxJPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        chanelJComboBoxJPanel.add(new JLabel("转发方式"));
        chanelJComboBox=new JComboBox<>(new Integer[]{Smctp.CHANNEL_DEBUG,Smctp.CHANNEL_VISUAL_SCOPE,Smctp.CHANNEL_MAX});
        chanelJComboBoxJPanel.add(chanelJComboBox);
        chanelJComboBox.addActionListener(this);
        chanelJComboBox.setRenderer((list,value,index,isSelected,cellHasFocus)->{
            JLabel jLabel=new JLabel();
            switch(value)
            {
                case Smctp.CHANNEL_DEBUG:
                    jLabel.setText("转发至调试器");
                    break;
                case Smctp.CHANNEL_VISUAL_SCOPE:
                    jLabel.setText("转发至示波器");
                    break;
                case Smctp.CHANNEL_MAX:
                    jLabel.setText("SMCTP转发");
                    break;
            }
            jLabel.setOpaque(true);
            return jLabel;
        });
        this.add(timeJLabel=new JLabel());
        new Thread(()->{
            try
            {
                while(true)
                {
                    timeJLabel.setText(timeFormatter(smctp.getLastUS()));
                    Thread.sleep(50);
                }
            }catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }).start();
        /*
          刷新串口号
          更新大小
          并设置所有的默认选项
         */
        chanelJComboBox.setSelectedItem(smctp.getDesignatedDataChannel());
    }

    /**
     * 动作事件
     * 处理内部所有面板的触发事件
     */
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(chanelJComboBox))
        {
            smctp.setDesignatedDataChannel(getSelectInteger(chanelJComboBox));
        }
    }

    /**
     * 获取下拉菜单选中的整数
     *
     * @param jComboBox 欲获取的下拉菜单
     * @return 整数
     * @throws NullPointerException 获取失败时抛出NullPointerException
     */
    private Integer getSelectInteger(JComboBox<Integer> jComboBox) throws NullPointerException
    {
        Object obj=jComboBox.getSelectedItem();
        if(obj instanceof Integer)
            return (Integer)obj;
        throw new NullPointerException();
    }
}
