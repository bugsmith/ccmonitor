package ccm.communicate.serial;

import ccm.common.*;
import ccm.communicate.common.Communicate;
import ccm.communicate.common.NotStartException;
import ccm.communicate.common.SendFailedException;
import ccm.component.input.InputJPanel;
import ccm.component.input.MultipleInputJPanel;
import ccm.component.output.OutputJPanel;
import ccm.component.override.TestJFrame;
import ccm.component.style.Style;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Test extends TestJFrame implements ActionListener, ReceiveDataListener, StatusChangeListener
{
    private static final String[]     title=new String[]{"communicate","serial","test"};
    private              Communicate  communicate;
    private              JButton      startJButton;
    private              InputJPanel  inputJPanel;
    private              OutputJPanel outputJPanel;

    public Test()
    {
        super(title);
        readConfig();
        setLayout(new BorderLayout());

        if(inputJPanel==null)
            inputJPanel=new MultipleInputJPanel();
        this.add(inputJPanel,BorderLayout.SOUTH);
        this.add(outputJPanel=new OutputJPanel(),BorderLayout.CENTER);
        try
        {
            inputJPanel.addDataReceiveListener(this);
            inputJPanel.addStatusChangeListener(this);
        }catch(TooManyListenerException e)
        {
            e.printStackTrace();
        }

        JPanel leftJPanel=new JPanel();
        this.add(leftJPanel,BorderLayout.EAST);
        leftJPanel.setLayout(new BorderLayout());

        if(communicate==null)
            communicate=new Serial();
        try
        {
            communicate.addReceiveDataListener(outputJPanel);
            communicate.addStatusChangeListener(this);
        }catch(TooManyListenerException ex)
        {
            ex.printStackTrace();
        }
        leftJPanel.add(communicate.getConfigJPanel(),BorderLayout.NORTH);


        JPanel buttonsJPanel=new JPanel();
        leftJPanel.add(buttonsJPanel,BorderLayout.CENTER);
        buttonsJPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        startJButton=new JButton("开始");
        buttonsJPanel.add(startJButton);
        startJButton.addActionListener(this);


    }

    public void readConfig()
    {
        try
        {
            ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream("configs/"+getTitleString()+".config"));
            communicate=(Communicate)objectInputStream.readObject();
            inputJPanel=(InputJPanel)objectInputStream.readObject();
            objectInputStream.readObject();
            objectInputStream.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        new Test().setVisible(true);
    }

    public String[] getTitleArray()
    {
        return title;
    }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(startJButton))
        {
            if(communicate.isOpen())
                communicate.stop();
            else
                communicate.start();
        }
    }

    public void receiveData(ReceiveDataEvent e)
    {
        if(e.getSource().equals(inputJPanel))
        {
            try
            {
                communicate.send(e.getData());
            }catch(NotStartException ex)
            {
                JOptionPane.showMessageDialog(Test.this,"未开启串口","错误",JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
            }catch(SendFailedException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void statusChange(StatusChangeEvent e)
    {
        if(e.getSource().equals(communicate))
        {
            outputJPanel.clear();
            if(e.getReason()==StatusChangeEvent.START_STOP)
                startJButton.setText(communicate.isOpen()?"停止":"开始");
        }
        else if(e.getReason()==StatusChangeEvent.CONFIG)
            writeConfig();
    }

    public void writeConfig()
    {
        System.out.println("writeConfig");
        try
        {
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(new FileOutputStream("configs/"+getTitleString()+".config"));
            objectOutputStream.writeObject(communicate);
            objectOutputStream.writeObject(inputJPanel);
            objectOutputStream.writeObject(new Style());
            objectOutputStream.close();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
    }

}
