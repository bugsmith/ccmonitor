package ccm.common;

import javax.swing.*;
import java.io.Externalizable;

public interface Configurable extends Externalizable
{
    /**
     * 获取配置用的面板
     *
     * @return 配置用的面板
     */
    JPanel getConfigJPanel();

}
