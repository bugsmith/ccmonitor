package ccm.common;

public interface StatusChangeListener
{
    /**
     * 状态变化事件
     */
    void statusChange(StatusChangeEvent event);
}
