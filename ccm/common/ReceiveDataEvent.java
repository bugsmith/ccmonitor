package ccm.common;

/**
 * 接收到数据
 */
public class ReceiveDataEvent extends CCMEvent
{
    protected final byte[] data;
    protected final long   timeUs;

    /**
     * @param source 事件源
     * @param data   数据
     */
    public ReceiveDataEvent(Object source,byte[] data)
    {
        this(source,data,0);
    }

    /**
     * @param source 事件源
     * @param data   数据
     * @param timeUs us时间戳
     */
    public ReceiveDataEvent(Object source,byte[] data,long timeUs)
    {
        super(source);
        this.data  =data;
        this.timeUs=timeUs;
    }

    /**
     * @param source 事件源
     * @param data   数据
     */
    public ReceiveDataEvent(Object source,String data)
    {
        this(source,data,0);
    }

    /**
     * @param source 事件源
     * @param data   数据
     */
    public ReceiveDataEvent(Object source,String data,long timeUs)
    {
        this(source,data.getBytes(),timeUs);
    }

    /**
     * 获取数据
     *
     * @return 数据
     */
    public byte[] getData()
    {
        return data;
    }

    public long getTimeUs()
    {
        return timeUs;
    }
}
