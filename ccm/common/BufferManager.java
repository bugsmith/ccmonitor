package ccm.common;

public class BufferManager
{
    public static int getUint8(int[] buf)
    {
        return getUint8(buf,0);
    }

    public static int getUint8(int[] buf,int i)
    {
        return (int)G(buf,i,0);
    }

    protected static long G(int[] buf,int i,int j)
    {
        return ((long)(buf[(i+j+buf.length)%buf.length]&0XFF))<<(j<<3);
    }

    public static int getUint8(byte[] buf)
    {
        return getUint8(buf,0);
    }

    public static int getUint8(byte[] buf,int i)
    {
        return (int)G(buf,i,0);
    }

    protected static long G(byte[] buf,int i,int j)
    {
        return ((long)(Byte.toUnsignedInt(buf[(i+j+buf.length)%buf.length])&0XFF))<<(j<<3);
    }

    public static int getUint16(int[] buf)
    {
        return getUint16(buf,0);
    }

    public static int getUint16(int[] buf,int i)
    {
        return (int)(G(buf,i,0)|G(buf,i,1));
    }

    public static int getUint16(byte[] buf)
    {
        return getUint16(buf,0);
    }

    public static int getUint16(byte[] buf,int i)
    {
        return (int)(G(buf,i,0)|G(buf,i,1));
    }

    public static long getUint32(int[] buf)
    {
        return getUint32(buf,0);
    }

    public static long getUint32(int[] buf,int i)
    {
        return (G(buf,i,0)|G(buf,i,1)|G(buf,i,2)|G(buf,i,3));
    }

    public static long getUint32(byte[] buf)
    {
        return getUint32(buf,0);
    }

    public static long getUint32(byte[] buf,int i)
    {
        return (G(buf,i,0)|G(buf,i,1)|G(buf,i,2)|G(buf,i,3));
    }

    public static long getUint64(int[] buf)
    {
        return getUint64(buf,0);
    }

    public static long getUint64(int[] buf,int i)
    {
        return G(buf,i,0)|G(buf,i,1)|G(buf,i,2)|G(buf,i,3)|G(buf,i,4)|G(buf,i,5)|G(buf,i,6)|G(buf,i,7);
    }

    public static long getUint64(byte[] buf)
    {
        return getUint64(buf,0);
    }

    public static long getUint64(byte[] buf,int i)
    {
        return G(buf,i,0)|G(buf,i,1)|G(buf,i,2)|G(buf,i,3)|G(buf,i,4)|G(buf,i,5)|G(buf,i,6)|G(buf,i,7);
    }

    public static int setUint8(int[] buf,int d)
    {
        return setUint8(buf,0,d);
    }

    public static int setUint8(int[] buf,int i,int d)
    {
        S(buf,i,0,d);
        return (i+1)%buf.length;
    }

    protected static void S(int[] buf,int i,int j,long d)
    {
        buf[(i+j+buf.length)%buf.length]=(int)(d>>(j<<3))&0XFF;
    }

    public static int setUint8(byte[] buf,int d)
    {
        return setUint8(buf,0,d);
    }

    public static int setUint8(byte[] buf,int i,int d)
    {
        S(buf,i,0,d);
        return (i+1)%buf.length;
    }

    protected static void S(byte[] buf,int i,int j,long d)
    {
        buf[(i+j+buf.length)%buf.length]=(byte)((int)(d>>(j<<3))&0XFF);
    }

    public static int setUint16(int[] buf,int d)
    {
        return setUint16(buf,0,d);
    }

    public static int setUint16(int[] buf,int i,int d)
    {
        S(buf,i,0,d);
        S(buf,i,1,d);
        return (i+2)%buf.length;
    }

    public static int setUint16(byte[] buf,int d)
    {
        return setUint16(buf,0,d);
    }

    public static int setUint16(byte[] buf,int i,int d)
    {
        S(buf,i,0,d);
        S(buf,i,1,d);
        return (i+2)%buf.length;
    }

    public static int setUint32(int[] buf,long d)
    {
        return setUint32(buf,0,d);
    }

    public static int setUint32(int[] buf,int i,long d)
    {
        S(buf,i,0,d);
        S(buf,i,1,d);
        S(buf,i,2,d);
        S(buf,i,3,d);
        return (i+4)%buf.length;
    }

    public static int setUint32(byte[] buf,long d)
    {
        return setUint32(buf,0,d);
    }

    public static int setUint32(byte[] buf,int i,long d)
    {
        S(buf,i,0,d);
        S(buf,i,1,d);
        S(buf,i,2,d);
        S(buf,i,3,d);
        return (i+4)%buf.length;
    }

    public static int setUint64(int[] buf,long d)
    {
        return setUint64(buf,0,d);
    }

    public static int setUint64(int[] buf,int i,long d)
    {
        S(buf,i,0,d);
        S(buf,i,1,d);
        S(buf,i,2,d);
        S(buf,i,3,d);
        S(buf,i,4,d);
        S(buf,i,5,d);
        S(buf,i,6,d);
        S(buf,i,7,d);
        return (i+8)%buf.length;
    }

    public static int setUint64(byte[] buf,long d)
    {
        return setUint64(buf,0,d);
    }

    public static int setUint64(byte[] buf,int i,long d)
    {
        S(buf,i,0,d);
        S(buf,i,1,d);
        S(buf,i,2,d);
        S(buf,i,3,d);
        S(buf,i,4,d);
        S(buf,i,5,d);
        S(buf,i,6,d);
        S(buf,i,7,d);
        return (i+8)%buf.length;
    }


}
