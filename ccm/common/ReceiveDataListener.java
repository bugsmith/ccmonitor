package ccm.common;

public interface ReceiveDataListener
{
    /**
     * 接收到数据事件
     */
    void receiveData(ReceiveDataEvent event);
}
