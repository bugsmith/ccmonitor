package ccm.common;

public class StatusChangeEvent extends CCMEvent
{
    public static final int UNKNOW    =0;
    public static final int START_STOP=1;
    public static final int RESTART   =1;
    public static final int CONFIG    =2;
    protected           int reason;

    /**
     * @param source 事件源
     */
    public StatusChangeEvent(Object source)
    {
        this(source,UNKNOW);
    }

    /**
     * @param source 事件源
     * @param reason 事件原因
     */
    public StatusChangeEvent(Object source,int reason)
    {
        super(source);
        this.reason=reason;
    }

    /**
     * 获取事件原因
     *
     * @return 事件原因
     */
    public int getReason()
    {
        return reason;
    }
}
