package ccm.common;

public final class Utils
{
    private Utils()
    {

    }

    public static String timeFormatter(long timeUs)
    {
        return String.format("%dh%02dm%02ds%03dms",timeUs/1000000/60/24,timeUs/1000000/60%24,timeUs/1000000%60,timeUs/1000%1000);
    }
}
