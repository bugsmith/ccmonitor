# 车车Monitor

![img.png](readme/img.png)

有问题发issues,欢迎PR

## 简介

一个低性能、不易于使用、与[SCNS](https://gitee.com/huoxingdawang/scns)配合的调试软件。
原本的用途是参加全国大学生智能汽车竞赛，但是在开发过程中逐步发现可以应用于一些奇奇怪怪的地方。

## 序

一个和智能车没啥关系的东西

为什么会有呢？

或许是为了驯化JAVA？

话说回来

真的和智能车没啥关系吗？

或许吧

## 功能简介

### 本体

![img.png](readme/CCMonitor.png)

### 字库生成器

![img.png](readme/GenerateFont.png)

### 贴图生成器

![img.png](readme/GenerateImage.png)

## 依赖的开源库

[JOGL](https://jogamp.org/jogl/www/)用于使用GPU加速示波器渲染。

[jSerialComm](https://github.com/Fazecast/jSerialComm)用于支持串口。

