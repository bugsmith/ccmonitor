package experiment;

public class HelloJNI
{
    static
    {
        System.loadLibrary("hello"); // Load native library at runtime
        // hello.dll (Windows) or libhello.so (Unixes)
    }

    // Test Driver
    public static void main(String[] args)
    {
        new HelloJNI().sayHello();  // invoke the native method
    }

    // Declare a native method sayHello() that receives nothing and returns void
    private native void sayHello();
}
